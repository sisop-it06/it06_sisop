#!/bin/bash

# check if the user provided an encrypted file as a command-line argument
if [ $# -eq 0 ]; then
    echo "Usage: $0 encrypted_file"
    exit 1
fi

# define alphabet array
alpha=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
cap=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# read the encrypted data file
encrypted_file="$1"
encrypted_data=$(cat "$encrypted_file")

# extract the hour from the encrypted file name
hour=${encrypted_file#*encrypt_}
hour=${hour%%:*}

# shift the alphabet and capital arrays based on the hour
shifted_alpha=()
shifted_cap=()
for (( i=0; i<26; i++ )); do
    idx=$(( (i + hour) % 26 ))
    shifted_alpha[i]=${alpha[idx]}
    shifted_cap[i]=${cap[idx]}
done

# decrypt the log data using the shifted alphabet
decrypted_data=$(echo "$encrypted_data" | tr "${shifted_alpha[*]}" "${alpha[*]}" | tr "${shifted_cap[*]}" "${cap[*]}")

# create backup file name with timestamp
backup_file="decrypt_${hour}_${encrypted_file#encrypt_${hour}:}"
backup_file=${backup_file//:/-} # replace ":" with "-" in the file name

# write the decrypted data to backup file
echo -e "$decrypted_data" > "$backup_file"
