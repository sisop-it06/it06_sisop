echo -e "\ntampilkan 5 universitas dengan ranking tertinggi di jepang: \n"
awk -F"," '$4 == "Japan" {print $1, $2, $3, $4, $5}' mesi.csv | head -5
echo -e " " 
echo -e "\ntampilkan universitas dengan FSR terendah: \n"
awk -F"," '$4 == "Japan" {print $9, $2, $3, $4}' mesi.csv | sort -g | head -n1
echo -e " "
echo -e "\ntampilkan 10 universitas ger rank tertinggi jepang: \n"
awk -F"," '$4 == "Japan" {print $20, $2}' mesi.csv | sort -n | head -n10
echo -e " "
echo -e "\ntampilkan universitas paling keren: \n"
awk -F"," '/Keren/ {print $2, $3, $4, $5}' mesi.csv
