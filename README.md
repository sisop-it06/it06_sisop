# sisop-praktikum-modul-1-2023-MHFD-IT06



- Fathika Afrine Azaruddin - 5027211016
- Athallah Narda Wiyoga - 5027211041
- Gilbert Immanuel Hasiholan - 5027211056

# Soal no. 1

#### 1.```echo -e "\ntampilkan 5 universitas dengan ranking tertinggi di jepang: \n"```

Baris ini menampilkan pesan "tampilkan 5 universitas dengan ranking tertinggi di jepang" pada terminal, yang diakhiri dengan karakter baris baru "\n".

#### ```awk -F"," '$4 == "Japan" {print $1, $2, $3, $4, $5}' mesi.csv | head -5```

Perintah ini terdiri dari empat bagian, yaitu:

- awk -F",": Perintah untuk menjalankan awk, yaitu sebuah tool untuk mengolah data dengan sintaks mirip dengan bahasa pemrograman. Argumen -F"," berfungsi untuk menentukan delimiter atau pemisah antar-kolom dalam file mesi.csv adalah tanda koma (,).
- '$4 == "Japan": Seleksi data berdasarkan kondisi kolom ke-4 sama dengan string "Japan".
- {print $1, $2, $3, $4, $5}: Mengambil kolom-kolom 1, 2, 3, 4, dan 5 dari data yang dipilih pada step 2.
- | head -5: Mengambil 5 data teratas.

Secara keseluruhan, kode tersebut digunakan untuk mengambil 5 data teratas dari file mesi.csv dengan kolom ke-4 bernilai "Japan". Data yang dipilih meliputi kolom 1 sampai 5, yaitu kolom untuk nama, usia, jenis kelamin, negara, dan gaji. Data tersebut kemudian ditampilkan sebagai output.

Jadi, perintah ini akan menampilkan daftar 5 universitas di Jepang dengan peringkat tertinggi.

#### 2. ```echo -e "\ntampilkan universitas dengan FSR terendah: \n"```

Perintah ini akan menampilkan pesan "tampilkan universitas dengan FSR terendah" pada terminal, yang diakhiri dengan karakter baris baru "\n".

#### ```awk -F"," '$4 == "Japan" {print $9, $2, $3, $4}' mesi.csv | sort -g | head -n1```

Perintah ini terdiri dari lima bagian, yaitu:

- awk -F",": Perintah untuk menjalankan awk, yaitu sebuah tool untuk mengolah data dengan sintaks mirip dengan bahasa pemrograman. Argumen -F"," berfungsi untuk menentukan delimiter atau pemisah antar-kolom dalam file mesi.csv adalah tanda koma (,).
- '$4 == "Japan": Seleksi data berdasarkan kondisi kolom ke-4 sama dengan string "Japan".
- {print $9, $2, $3, $4}: Mengambil kolom-kolom 9, 2, 3, dan 4 dari data yang dipilih pada step 2.
- | sort -g: Mengurutkan data secara numerik (-g) berdasarkan nilai pada kolom pertama (kolom ke-9).
- | head -n1: Mengambil data teratas.

Secara keseluruhan, kode tersebut digunakan untuk mengambil data dengan nilai terkecil dari file mesi.csv dengan kolom ke-4 bernilai "Japan". Data yang dipilih hanya berisi kolom 9, 2, 3, dan 4. Data tersebut kemudian diurutkan berdasarkan kolom ke-9 secara numerik dan hanya data teratas yang ditampilkan sebagai output.

Jadi, perintah ini akan menampilkan universitas di Jepang dengan FSR (rasio dosen-mahasiswa) terendah.

#### 3. ```echo -e "\ntampilkan 10 universitas ger rank tertinggi jepang: \n"```

Perintah ini akan menampilkan pesan "tampilkan 10 universitas ger rank tertinggi jepang:" pada terminal, yang diakhiri dengan karakter baris baru "\n". Tujuannya adalah untuk memberikan informasi tentang hasil yang akan ditampilkan selanjutnya.

#### ```awk -F"," '$4 == "Japan" {print $20, $2}' mesi.csv | sort -n | head -n10```

Perintah ini terdiri dari lima bagian, yaitu:

- awk -F",": Perintah untuk menjalankan awk, yaitu sebuah tool untuk mengolah data dengan sintaks mirip dengan bahasa pemrograman. Argumen -F"," berfungsi untuk menentukan delimiter atau pemisah antar-kolom dalam file mesi.csv adalah tanda koma (,).
- '$4 == "Japan": Seleksi data berdasarkan kondisi kolom ke-4 sama dengan string "Japan".
- {print $20, $2}: Mengambil kolom 20 dan 2 dari data yang dipilih pada step 2.
- | sort -n: Mengurutkan data secara numerik (-n) berdasarkan kolom pertama (kolom ke-20).
- | head -n10: Mengambil 10 data teratas.

Secara keseluruhan, kode tersebut digunakan untuk mengambil 10 data teratas dari file mesi.csv dengan kolom ke-4 bernilai "Japan", dan diurutkan berdasarkan kolom ke-20 secara numerik. Hanya kolom ke-20 dan ke-2 yang dicetak dan ditampilkan sebagai output.

#### 4. ```echo -e "\ntampilkan universitas paling keren: \n"```

Perintah ini akan menampilkan pesan "tampilkan universitas paling keren" pada terminal, yang diakhiri dengan karakter baris baru "\n".

#### ```awk -F"," '/Keren/ {print $2, $3, $4, $5}' mesi.csv```

Perintah ini terdiri dari empat bagian, yaitu:

- awk -F",": Perintah untuk menjalankan awk, yaitu sebuah tool untuk mengolah data dengan sintaks mirip dengan bahasa pemrograman. Argumen -F"," berfungsi untuk menentukan delimiter atau pemisah antar-kolom dalam file mesi.csv adalah tanda koma (,).
- '/Keren/: Seleksi data berdasarkan kondisi dimana kolom manapun dalam baris tersebut mengandung string "Keren".
- {print $2, $3, $4, $5}: Mengambil kolom-kolom 2, 3, 4, dan 5 dari data yang dipilih pada step 2.
- mesi.csv: Nama file yang diolah.

Secara keseluruhan, kode tersebut digunakan untuk mengambil data pada file mesi.csv yang mengandung string "Keren" pada salah satu kolomnya.

https://ibb.co/42XtR6F

https://ibb.co/nfLH72s

# Soal no. 2

```shell
#!/bin/bash
```

Baris ini merupakan shebang, yang menunjukkan bahwa script ini akan dijalankan dengan menggunakan Bash shell.

```shell
dir_folder () {
  ```

Baris ini merupakan fungsi untuk membuat folder dan mengunduh gambar dari internet.

```shell
jam=$(date +"%H")
```

Baris ini akan mendapatkan waktu saat ini dalam format jam (dalam angka) menggunakan perintah _date_ dan menyimpannya ke dalam variabel _jam_.

```shell
if (( $jam >= 0 && $jam < 2 )); then
  jumlah_gambar=1
else
  jumlah_gambar=$((jam-1))
fi
```

Baris ini akan menentukan jumlah gambar yang akan didownload, tergantung pada waktu saat ini. Jika waktu saat ini antara pukul 00.00 dan 01.59, maka jumlah gambar akan diatur ke 1. Jika tidak, maka jumlah gambar akan diatur sejumlah jam saat ini dikurangi 1.

```shell
nomor_folder=$(ls -1d kumpulan_* 2>/dev/null | wc -l)
let nomor_folder++
nama_folder="kumpulan_$nomor_folder"
mkdir "$nama_folder"
```

Baris ini akan membuat sebuah folder baru untuk menyimpan gambar-gambar yang akan didownload. Pertama-tama, script akan menghitung jumlah folder yang sudah ada dengan pola nama kumpulan_* menggunakan perintah _ls_ dengan argumen _-1d_. Jika tidak ada folder yang ditemukan, maka output dari perintah _ls_ akan diabaikan menggunakan _2>/dev/null_. Jumlah folder yang ditemukan kemudian dihitung menggunakan perintah _wc -l_ dan disimpan dalam variabel nomor_folder. Selanjutnya, nomor folder tersebut akan ditambahkan 1 dan disimpan dalam variabel **nomor_folder** lagi. Nama folder baru kemudian dibentuk dengan pola **kumpulan_$nomor_folder** dan disimpan dalam variabel **nama_folder**. Akhirnya, script akan membuat folder baru dengan nama yang telah dibentuk menggunakan perintah _mkdir_.

```shell
for ((i=1;i<=jumlah_gambar;i++)); do
  nomor_file=$(ls -1 $nama_folder/perjalanan_* 2>/dev/null | wc -l)
  let nomor_file++
  wget -q "https://source.unsplash.com/1600x900/?Indonesia" -O "$nama_folder/perjalanan_$nomor_file.jpg"
done
```

Baris ini akan mendownload gambar-gambar dari situs Unsplash menggunakan perintah _wget_. Setiap gambar akan diunduh dengan URL ***https://source.unsplash.com/1600x900/?Indonesia*** dan disimpan dalam folder **nama_folder**. Script akan melakukan loop sebanyak **jumlah_gambar** kali, dan untuk setiap iterasi, script akan mengambil nomor file terakhir dari file yang memiliki pola nama perjalanan_* di dalam folder **nama_folder**. Jumlah nomor file tersebut kemudian ditambahkan 1 dan disimpan dalam variabel **nomor_file**. Nama file baru kemudian dibentuk dengan pola **perjalanan_$nomor_file**.jpg dan digunakan sebagai nama file yang akan diunduh.

https://ibb.co/7XRRXxd

```shell
zip_folder () {
```

Baris ini merupakan fungsi untuk mengompres folder menjadi file ZIP.

```shell
nomor_zip=$(ls -1d devil_* 2>/dev/null | wc -l)
((nomor_zip++))

zip_file="devil_$nomor_zip.zip"
```

Pada baris ini, variabel **nomor_zip** akan diisi dengan jumlah file ZIP yang ada di direktori saat ini yang dimulai dengan **devil_**. Kemudian variabel tersebut akan diincrement dan digunakan untuk menentukan nama file ZIP baru.

```shell
for dir in kumpulan_*/; do
  dir=${dir%/}
  if [[ $dir = *"kumpulan_"* ]]; then
    zip -qr "$zip_file" "$dir"
  fi
done
```
Baris ini merupakan sebuah loop for yang digunakan untuk mengompres setiap folder yang dimulai dengan **kumpulan_** menjadi file ZIP yang baru dibuat pada langkah sebelumnya. Variabel _dir_ akan diisi dengan nama setiap folder yang dimulai dengan **kumpulan_**. Variabel tersebut akan di-strip dari tanda slash (/) di akhir dan digunakan untuk memeriksa apakah nama folder tersebut dimulai dengan **kumpulan_**. Jika ya, perintah zip akan digunakan untuk mengompres folder tersebut dan menyimpannya dalam file ZIP yang baru dibuat pada langkah sebelumnya.

```shell
if [ "$1" == "dir_folder" ]; then
    dir_folder
elif [ "$1" == "zip_folder" ]; then
    zip_folder
else
    echo "Invalid argument"
    exit 1
fi
```
Baris selanjutnya merupakan logika if-else yang digunakan untuk menentukan fungsi mana yang akan dijalankan berdasarkan argumen yang diberikan. Jika argumen adalah **dir_folder**, maka fungsi **dir_folder** akan dijalankan. Jika argumen adalah **zip_folder**, maka fungsi **zip_folder** akan dijalankan. Jika argumen tidak valid, maka pesan _Invalid argument_ akan dicetak dan script akan keluar dengan status kode 1.
https://ibb.co/vwSmXTv


```shell
# crontab -e
# 0 */6 * * * ~/kobeni_liburan.sh dir_folder
# 0 0 * * * ~/kobeni_liburan.sh zip_folder
```

Baris terakhir merupakan cronjob yang dapat digunakan untuk menjalankan script bash ini secara otomatis. Cronjob pertama akan menjalankan fungsi **dir_folder** setiap 6 jam sekali. Cronjob kedua akan menjalankan fungsi **zip_folder** setiap hari pukul 00:00.

# Soal no. 3

## script louis.sh

#### ```#!/bin/bash```

Code di atas merupakan sebuah script Bash yang melakukan proses pendaftaran pengguna (user) baru pada sistem dengan beberapa kondisi validasi.

#### ```echo "Selamat datang di sistem register"```

Baris ini mencetak pesan selamat datang ke layar.

#### ```printf "Masukkan username: " read -r  username```

Mengambil input dari pengguna (username) menggunakan read. Pesan pada read digunakan untuk meminta input dari pengguna.

#### ```printf "Masukkan password: " read -rs password```

Mengambil input dari pengguna (password) menggunakan read. Pesan pada read digunakan untuk meminta input dari pengguna.

#### ```if [[ ${#password} -lt 8 ]] || ! [[ "$password" =~ [a-z] ]] || ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [0-9] ]] || [[ "$password" == *"$username"* ]] || [[ "$password" =~ "chicken" ]] || [[ "$password" =~ "ernie" ]]; then```
    echo "Password tidak memenuhi kriteria yang ditentukan"
    exit 1

Baris ini melakukan beberapa validasi pada password yang dimasukkan oleh pengguna. Validasi ini meliputi:
- Password harus memiliki panjang minimal 8 karakter.
- Password harus terdiri dari huruf kecil, huruf besar, dan angka.
- Password tidak boleh mengandung username.
- Password tidak boleh mengandung kata "chicken" atau "ernie".
Jika password tidak memenuhi kriteria validasi tersebut, script akan mencetak pesan error dan menghentikan proses pendaftaran.

#### ```if grep -q "^$username:" /home/kali/soal3/users/users.txt; then```

Baris ini memeriksa apakah username yang dimasukkan oleh pengguna sudah ada dalam file /home/kali/soal3/users/users.txt. Jika username sudah terdaftar, script akan mencetak pesan error dan menghentikan proses pendaftaran.

#### ```echo "$username:$password" >> /home/kali/soal3/users/users.txt```

Baris ini menambahkan username dan password baru ke dalam file /home/kali/soal3/users/users.txt.

#### ```echo "User $username berhasil didaftarkan"```

Baris ini mencetak pesan berhasil kepada pengguna.

#### ```echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> log.txt```

Baris ini mencatat ke file log.txt bahwa pengguna berhasil mendaftar. Log tersebut mencantumkan tanggal dan waktu pendaftaran, serta username yang berhasil didaftarkan.

https://ibb.co/r2v9WDR

## script retep.sh

#### ```#!/bin/bash```

Code di atas merupakan sebuah script bash untuk melakukan autentikasi login pada sebuah sistem.

#### ```echo "Selamat datang di sistem login"```

Menampilkan pesan selamat datang di sistem login dengan menggunakan perintah echo.

#### ```printf "Masukkan username: " read -r  username```
#### ```printf "Masukkan password: " read -rs password```

Meminta input dari pengguna berupa username dan password menggunakan perintah read.

#### ```if ! grep -q "^$username:" /home/kali/soal3/users/users.txt; then```
    echo "User tidak terdaftar"
    echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1

Mengecek apakah user yang dimasukkan sudah terdaftar dengan memeriksa apakah username tersebut terdapat di dalam file /home/kali/soal3/users/users.txt. Jika tidak ditemukan, maka program akan menampilkan pesan "User tidak terdaftar" dan menuliskan informasi login error pada file log.txt menggunakan perintah echo dan >>.

#### ```saved_password=$(grep "^$username:" /home/kali/soal3/users/users.txt | cut -d: -f2) if [[ "$password" != "$saved_password" ]]; then```
    echo "Password salah"
    echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1

Jika user sudah terdaftar, maka program akan memeriksa apakah password yang dimasukkan benar dengan membandingkan password yang dimasukkan dengan password yang tersimpan pada file /home/kali/soal3/users/users.txt. Jika password salah, maka program akan menampilkan pesan "Password salah" dan menuliskan informasi login error pada file log.txt menggunakan perintah echo dan >>.

Jika ada error pada proses autentikasi, program akan keluar dan mengembalikan exit code 1.

#### ```echo "Selamat datang $username"```
#### ```echo "$(date +"%y/%m/%d %H:%M:%S") LOGIN: INFO User $username logged in" >> log.txt```

Jika username dan password sudah benar, maka program akan menampilkan pesan "Selamat datang <username>" dan menuliskan informasi login sukses pada file log.txt menggunakan perintah echo dan >>.

https://ibb.co/Cv0QGNG

# Soal no. 4

## Encrypt

#### `#!/bin/bash`

Script ini dijalankan menggunakan bash shell.

#### `time=$(date "+%H:%M")`
#### `date=$(date "+%d:%m:%Y")`

Script mendapatkan waktu saat ini menggunakan perintah date dan menyimpan waktu saat ini dalam variabel time dalam format jam:menit dan menyimpan tanggal dalam variabel date dalam format tanggal:bulan:tahun.

#### `alpha=(a b c d e f g h i j k l m n o p q r s t u v w x y z)`
#### `cap=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)`

Array alpha dan cap berisi alfabet dan huruf besar bahasa Inggris.

#### `hr=$(date "+%H")`
#### `hr=${hr#0}`

Variabel hr menyimpan jam saat ini dalam format dua digit. Baris kedua digunakan untuk menghapus angka nol pada awal string jika ada.

`shifted_alpha=()
shifted_cap=()
for (( i=0; i<26; i++ )); do
    idx=$(( (i + hr) % 26 ))
    shifted_alpha[i]=${alpha[idx]}
    shifted_cap[i]=${cap[idx]}
done`

Array *shifted_alpha* dan *shifted_cap* dibuat dengan menggeser alfabet dan huruf besar sebanyak *hr* posisi. Array-array ini akan digunakan untuk mengenkripsi data log.

#### `logfile="/var/log/syslog"`
#### `logdata=$(cat -v "$logfile")`

Variabel *logfile* menyimpan alamat file log sistem dan variabel *logdata* menyimpan isi file log.

#### `encrypted_data=$(echo "$logdata" | tr "${alpha[*]}" "${shifted_alpha[*]}" | tr "${cap[*]}" "${shifted_cap[*]}")`

Variabel *encrypted_data* menyimpan isi file log yang sudah dienkripsi dengan cara mengganti setiap huruf di *logdata* dengan huruf yang digeser sebanyak *hr* posisi menggunakan array *shifted_alpha* dan *shifted_cap*.

#### `backup_file="encrypt_${time}_${date}.txt"`

Variabel *backup_file* menyimpan nama file cadangan yang akan dibuat. Nama file ini terdiri dari teks "encrypt_", diikuti oleh waktu dan tanggal saat ini yang disimpan dalam format *time* dan *date*, dan diakhiri dengan ekstensi file ".txt".

#### `echo -e  "$encrypted_data" > "/home/kali/soal4/$backup_file"`

Data yang sudah dienkripsi disimpan ke dalam file baru dengan nama yang disimpan dalam variabel *backup_file* pada direktori */home/kali/soal4/*.

#### `# crontab : 0 */2 * * * /home/kali/soal4/log_encrypt.sh`

Script ini harus dijalankan secara teratur. Perintah crontab digunakan untuk menjalankan script ini setiap 2 jam.

https://ibb.co/G5DHx73

## Decrypt

#### `#!/bin/bash`

Baris ini merupakan shebang, yang menunjukkan bahwa script ini akan dijalankan dengan menggunakan Bash shell.

`if [ $# -eq 0 ]; then
    echo "Usage: $0 encrypted_file"
    exit 1
fi`

Baris ini mengecek apakah ada argumen yang diberikan saat menjalankan script ini. Jika tidak ada argumen, maka script ini akan menampilkan pesan error dan keluar dari program dengan exit status 1. Argumen yang diharapkan adalah file yang telah dienkripsi.

#### `alpha=(a b c d e f g h i j k l m n o p q r s t u v w x y z)`
#### `cap=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)`

Baris ini mendefinisikan dua buah array yang akan digunakan untuk menyimpan abjad kecil dan besar.

#### `encrypted_file="$1"`
#### `encrypted_data=$(cat "$encrypted_file")`

Baris ini mengambil file yang telah dienkripsi dari argumen pertama yang diberikan saat menjalankan script dan membacanya dengan menggunakan perintah *`cat`. Hasilnya disimpan dalam variabel **`encrypted_data`*.

#### `hour=${encrypted_file#*encrypt_}`
#### `hour=${hour%%:*}`

Baris ini mengambil jam pada waktu file dienkripsi dari nama file. Nama file harus mengikuti format *`encrypt_JAM:00_file.txt`. Perintah **`${encrypted_file#*encrypt_}`* digunakan untuk menghapus bagian *`encrypt_`* pada nama file. Sedangkan *`${hour%%:*}`* digunakan untuk menghapus bagian setelah *`:`* pada nama file, sehingga yang tersisa hanya jam pada waktu enkripsi.

`shifted_alpha=()
shifted_cap=()
for (( i=0; i<26; i++ )); do
    idx=$(( (i + hour) % 26 ))
    shifted_alpha[i]=${alpha[idx]}
    shifted_cap[i]=${cap[idx]}
done`

Baris ini digunakan untuk menggeser array abjad kecil dan besar sebanyak jam yang didapatkan dari nama file. Proses penggeseran dilakukan dengan menggunakan loop *`for`. Setiap karakter pada array di-shift sebanyak **`hour`* mod 26, dan hasilnya disimpan dalam array *`shifted_alpha`* dan *`shifted_cap`*.

#### `decrypted_data=$(echo "$encrypted_data" | tr "${shifted_alpha[*]}" "${alpha[*]}" | tr "${shifted_cap[*]}" "${cap[*]}")`

Baris ini mendekripsi data yang telah dienkripsi dengan mengganti setiap karakter yang ada di dalam *`encrypted_data`* dengan karakter yang sesuai di dalam array *`alpha`* atau *`cap`. Proses ini dilakukan dengan menggunakan perintah **`tr`. Karakter-karakter yang ada di dalam **`encrypted_data`* yang cocok dengan karakter di dalam array *`shifted_alpha`* atau *`shifted_cap`* akan diganti dengan karakter yang cocok di dalam array *`alpha``* atau *`cap`*.

https://ibb.co/Wgns9L7








